# A função ptBezier_otimizado calcula pontos utilizando o algotirmo de de Casteljau, a partir de uma matriz de pontos de controle. 
# O retorno da função é um único ponto, que pertence à Bezier para um dado t.
#Trata-se de uma otimização, pois os pontos encontrados durante o cálculo são descartados a fim de liberar espaço para os seguintes.


function return_ptBezier = ptBezier_otimizado(A, t)
    [linha coluna] = size(A);
    B(:,:) = A;
    for i = 2:linha
        for j = 1:(linha-i+1)
            B(j,:) = (1-t) .* B(j,:) + B(j+1,:) .* t;
        endfor;
    endfor;
    return_ptBezier = B(1,:);
end
