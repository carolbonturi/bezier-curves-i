# This function subdivides a Bezier curve into two segments (left and right)
# The curve is cut at the chosen parameter value t
# subdivision_aux (A, t, side)
#       A is the control points matrix
#           A(N,2) where the first column is occupied by x, and the second by y
#           t is a given floating point between 0 and 1
#           if the side of interest is the right hand side, chose side equal to 0.
#                Beside, if the interest is the left side, chose side equal to 1.
#       return The return matrix corresponds to the new control points of the subdivision.



function return_divide = subdivision(A, t, side)

B = deCasteljau_aux(A,t);
A1 = zeros(length(A), 2);
A2 = zeros(length(A), 2);

    for i = 1:length(A)
        A1(i, :) = B(1, :, i);
        A2(i, :) = B(i, :, length(A)-i+1);
    endfor;

if(side == 0)
    return_divide = A1;
else
    return_divide = A2;
endif


end
