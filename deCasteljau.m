# This function is the de Casteljau algorithm implementation. Therefore, it allows to evaluate a point on a Bezier curve given the parameter t [0,1]
# deCasteljau (A, t)
#       A is the control points matrix
#           A(N,2) where the first column is occupied by x, and the second by y
#           t is a given floating point between 0 and 1
#       return the vector B => [ans x, ans y]




function return_deCasteljau = deCasteljau(A, t)
    [linha coluna] = size(A);
    B(:,:,1) = A;  # inicializa o primeiro plano de B, que  igual à matriz A
    for i = 2:linha
        for j = 1:(linha-i+1)
            B(j,:,i) = (1-t) .* B(j,:,i-1) + B(j+1,:,i-1) .* t;
        endfor;
    endfor;
    return_deCasteljau = max(B(:,:,linha));
end
