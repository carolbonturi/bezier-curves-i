# Curvas de Bézier Cúbicas
#Esta função faz o gráfico de curvas de Bézier cúbicas, e de seu polígono de controle

b0 = [0, 0];
b1 = [0, 0];
b2 = [0, 0];
b3 = [0, 0];

b0 = input("Informe as coordenadas de b0 no formato [x0, y0]\n");
b1 = input("Informe as coordenadas de b1 no formato [x1, y1]\n");
b2 = input("Informe as coordenadas de b2 no formato [x2, y2]:\n");
b3 = input("Informe as coordenadas de b3 no formato [x3, y3]:\n");

t = linspace(0, 1, 1000);
x = b0(1).*(1-t).^3 + b1(1).*3.*(1-t).^2.*t + b2(1)*3.*(1-t).*t.*t + b3(1).*t.*t.*t;
y = b0(2).*(1-t).^3 + b1(2).*3.*(1-t).^2.*t + b2(2)*3.*(1-t).*t.*t + b3(2).*t.*t.*t;

xb0b1 = b0(1) + t.*(b1(1) - b0(1));
yb0b1 = b0(2) + t.*(b1(2) - b0(2));

xb1b2 = b1(1) + t.*(b2(1) - b1(1));
yb1b2 = b1(2) + t.*(b2(2) - b1(2));

xb2b3 = b2(1) + t.*(b3(1) - b2(1));
yb2b3 = b2(2) + t.*(b3(2) - b2(2));

figure;
plot(xb0b1, yb0b1, x, y);
set (gca, "xaxislocation", "zero");
hold on;
plot(xb1b2, yb1b2);
plot(xb2b3, yb2b3);

