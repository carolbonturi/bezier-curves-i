# This function plots a Quadratic Bézier Curve inside its control polygon
# plot2 (b0, b1, b2)
#       b0, b1 and b2 are points, with (x, y) coordinates -> vector argument
#       b0 = [x0 y0]; b1 = [x1 y1]; b2 = [x2 y2]
#       return a figure => the Bézier curve and the control polygon filled red


function [xb0b1, yb0b1, x, y, xb1b2, yb1b2] = plot2(b0, b1, b2)

t = linspace(0, 1, 101);
x = b0(1).*(1-t).^2 + b1(1).*2.*(1-t).*t + b2(1)*t.*t;
y = b0(2).*(1-t).^2 + b1(2).*2.*(1-t).*t + b2(2)*t.*t;

xb0b1 = b0(1) + t.*(b1(1) - b0(1));
yb0b1 = b0(2) + t.*(b1(2) - b0(2));

xb1b2 = b1(1) + t.*(b2(1) - b1(1));
yb1b2 = b1(2) + t.*(b2(2) - b1(2));

X = [ xb0b1 xb1b2 ];
Y = [ yb0b1 yb1b2 ];


figure;
fill(X,Y,'r');
hold on
h = plot(x,y);
grid on;
title ('Quadratic Bezier curve and control polygon')
set (gca, "xaxislocation", "zero")
holf off

end
