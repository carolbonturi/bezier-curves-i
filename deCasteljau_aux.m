# This function is the de Casteljau algorithm implementation. Therefore, it allows to evaluate a point on a Bezier curve given the parameter t [0,1]
# deCasteljau (A, t)
#       A is the control points matrix
#           A(N,2) where the first column is occupied by x, and the second by y
#           t is a given floating point between 0 and 1
#       return The matrix generated B is used as support for other functions.




function return_deCasteljau_aux = deCasteljau_aux(A, t)
    [linha coluna] = size(A);
    B(:,:,1) = A;
    for i = 2:linha
        for j = 1:(linha-i+1)
            B(j,:,i) = (1-t) .* B(j,:,i-1) + B(j+1,:,i-1) .* t;
        endfor;
    endfor;
    return_deCasteljau_aux = B(:,:,:);
end
