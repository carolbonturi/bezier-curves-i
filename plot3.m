# This function plots a Cubic Bézier Curve inside its control polygon
# plot3 (b0, b1, b2, b3)
#       b0, b1, b2 and b3 are points, with (x, y) coordinates -> vector argument
#       b0 = [x0 y0]; b1 = [x1 y1]; b2 = [x2 y2]; b3 = [x3 y3]
#       return a figure => the Bézier curve and the control polygon filled red


function [xb0b1, yb0b1, x, y, xb1b2, yb1b2, xb2b3, yb2b3] = plot2(b0, b1, b2, b3)

t = linspace(0, 1, 101);
x = b0(1).*(1-t).^3 + b1(1).*3.*(1-t).^2.*t + b2(1)*3.*(1-t).*t.*t + b3(1).*t.*t.*t;
y = b0(2).*(1-t).^3 + b1(2).*3.*(1-t).^2.*t + b2(2)*3.*(1-t).*t.*t + b3(2).*t.*t.*t;

xb0b1 = b0(1) + t.*(b1(1) - b0(1));
yb0b1 = b0(2) + t.*(b1(2) - b0(2));

xb1b2 = b1(1) + t.*(b2(1) - b1(1));
yb1b2 = b1(2) + t.*(b2(2) - b1(2));

xb2b3 = b2(1) + t.*(b3(1) - b2(1));
yb2b3 = b2(2) + t.*(b3(2) - b2(2));


X = [ xb0b1 xb1b2 xb2b3];
Y = [ yb0b1 yb1b2 yb2b3];


figure;
fill(X,Y,'r');
hold on
h = plot(x,y);
grid on;
title ('Cubic Bezier curve and control polygon')
set (gca, "xaxislocation", "zero")
holf off

end

